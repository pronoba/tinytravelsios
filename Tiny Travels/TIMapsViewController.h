//
//  TIMapsViewController.h
//  Tiny Travels
//
//  Created by Pronob Ashwin on 6/12/14.
//  Copyright (c) 2014 Pronob Ashwin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface TIMapsViewController : UIViewController <UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapsView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end
