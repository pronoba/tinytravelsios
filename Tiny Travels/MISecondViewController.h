//
//  MISecondViewController.h
//  Tiny Travels
//
//  Created by Pronob Ashwin on 6/12/14.
//  Copyright (c) 2014 Pronob Ashwin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISecondViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
