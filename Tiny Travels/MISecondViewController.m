//
//  MISecondViewController.m
//  Tiny Travels
//
//  Created by Pronob Ashwin on 6/12/14.
//  Copyright (c) 2014 Pronob Ashwin. All rights reserved.
//

#import "MISecondViewController.h"

@interface MISecondViewController ()

@end

@implementation MISecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURL *url = [NSURL URLWithString:@"http://tinytravels.com/guides.html"];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    [self.webView loadRequest:request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
