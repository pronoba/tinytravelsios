//
//  MIFirstViewController.m
//  Tiny Travels
//
//  Created by Pronob Ashwin on 6/12/14.
//  Copyright (c) 2014 Pronob Ashwin. All rights reserved.
//

#import "MIFirstViewController.h"

@interface MIFirstViewController ()

@end

@implementation MIFirstViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSURL *url = [NSURL URLWithString:@"http://tinytravels.com/discover.html"];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    
    [self.webView loadRequest:request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
